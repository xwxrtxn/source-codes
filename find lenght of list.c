// forma interativa

int getcount(struct node* head)
{
	int count = 0;
	struct node* current = head;
	while(current != NULL)
	{
		current = current -> next;
		count++;
	}
	return count;
}

//============================================================================//
//forma recursiva
int getcount(struct node* head)
{
	if(head == NULL)
	{
		return 0;
	}
	else
	{
		return 1 + getcount(head -> next);
	}
}