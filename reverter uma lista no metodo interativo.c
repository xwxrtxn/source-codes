// reverter uma lista

struct node
{
	int data;
	struct node* next;
};

void reverse(struct node* head)
{
	struct node *current, *prev, *next;
	current = head;
	prev = NULL;
	prev = NULL;
	
	while(current != NULL)
	{
		next = current -> next;
		current -> next = prev;
		prev = current;
		current = next;
	}
	head = prev;
	return head;
}