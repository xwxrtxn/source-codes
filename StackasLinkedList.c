// implementação como lista encadeada

struct Stacknode
{
	int data;
	struct Stacknode* next;
};

struct Stacknode* new_node(int data)
{
	struct Stacknode* stack = (struct Stacknode*)malloc(sizeof(struct Stacknode));
	
	stack -> data = data;
	stack -> next = NULL;
	return stack;
}

int isEmpty(struct Stacknode* root)
{
	return !root;
}

void add_stack(struct Stacknode** root, int data)
{
	struct Stacknode* stack = new_node(data);
	stack -> next = *root;
	*root = stack;
	
}

int pop(struct Stacknode** root)
{
	if(isEmpty(*root))
		return INT_MIN;
	struct Stacknode* temp = *root;
	*root =(*root) -> next;
	int popped = temp -> data;
	free(temp);
	
	return popped;
}

int top(struct Stacknode** root)
{
	if(isEmpty(*root))
	{
		return INT_MIN;
	}
	return root -> data;
}