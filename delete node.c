
// **head_ref referencia do inicio da lista (ponteiro de ponteiro)
// dele��o dada uma chave
void delete_node(struct node **head_ref, int key)
{
	struct node* temp = *head_ref, *prev;
	
	if(temp != NULL && temp -> data == key)
	{
		*head_ref = temp -> next;
		
		free(temp);
		
		return;
	}
	
	while(temp != NULL && temp -> data != key)
	{
		prev = temp;
		temp = temp -> next;
	}
	if(temp == NULL) return;
	
	else
	{
		prev -> next = temp -> next;
	}
	free(temp);
}

//============================================================================//
// dele��o dada uma posi��o
void delete_node(struct node **head_ref, int position, int i)
{
	if(*head_ref == NULL)
	{
		return;
	}
	struct node* temp = *head_ref;
	
	if(position == 0)
	{
		*head_ref = temp -> next;
		free(temp);
		return;
	}
	
	for(i = 0; temp != NULL && i < position - 1; i++)
	{
		temp = temp -> next;
	}
	
	if(temp == NULL || temp -> == NULL)
	{
		return;
	}
	struct node *next = temp -> next -> next;
	
	free(temp -> next);
	
	temp -> next = next;
}

//============================================================================//
//deletar lista completa
void delete_list(struct node** head_ref)
{
	struct node* current = *head_ref;
	struct node* next;
	
	while(current != NULL)
	{
		next =current -> next;
		free(current);
		current -> next;
	}
	
	*head_ref = NULL;
}
//============================================================================//

void delete_nth_node(int n, int i)
{
	struct node* temp = head;
	
	if(n == 1) // primeiro no
	{
		head = temp -> next;
		free(temp)
		return;
	}
	for(i = 0; i < n -2; i++)
		temp = temp -> next
	struct node* temp2 = temp -> next;
	temp -> next = temp2 -> next;
	free(temp2);
	return;
	
}