// adicionar n�s no inicio

void push(struct node** head_ref, int new_data)
{
	struct node* new_node = (struct node*)malloc(sizeof(struct node));
	
	new_node -> data = new_data;
	
	new_node -> next = (*head_ref);
	
	(*head_ref) = new_node;
}

//=================================================================================//
// no meio//
void insert_after(struct node* prev_node. int new_data)
{
	if(prev_node == NULL)
	{
		return;
	}
	
	struct node* new_node = (struct node*)malloc(sizeof(struct node));
	
	new_node->data  = new_data;
	
	new_node->next = prev_node->next;
	
	prev_node->next = new_node;
}

//=================================================================================//
// no fim//
void append(struct node** head_ref, int new_data)
{
	struct node* new_node = (struct node*)malloc(sizeof(struct node);
	
	struct node *last = *head_ref;
	
	new_node -> data = new_data;
	
	new_node -> next = NULL;
	
	if(*head_ref == NULL)
	{
		*head_ref = new_node;
		return;
	}
	while (last->next != NULL) 
        last = last->next; 
   
    last->next = new_node; 
    
    return;     
} 
}