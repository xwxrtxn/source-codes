void removeNode(struct node** head_ref, struct node* del)
{
	if(*head_ref == NULL || del == NULL)
	{
		return; // caso base qnd a lista esta vazia;
	}
	
	if(*head_ref == del)
	{
		*head_ref = del -> next;
	}
	
	if(del -> next != NULL)
	{
		del -> next -> prev = del -> prev;
	}
	
	if(del -> prev != NULL)
	{
		del -> prev -> next = del -> next;
	}
	
	free(del);
	return;
}