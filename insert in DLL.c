// insere na frente

struct node
{
	int data;
	struct node* next;
	struct node* prev;
};

void insertAtHead(struct node** head_ref, int new_data)
{
	struct node* new_node = (struct node*)malloc(sizeof (struct node));
	
	new_node -> data = new_data;
	
	new_node -> next = (*head_ref);
	
	new_node -> prev = NULL;
	
	if((*head_ref) != NULL)
		(*head_ref) -> prev = new_node;
	
	(*head_ref) = new_node;
}

//============================================================================//
// insere depois de um no fornecido
void insertAfter(struct node* prev_node, int new_data)
{
	if(prev_node == NULL)
	{
		return;
	}
	
	struct node* new_node = (struct node*)malloc(sizeof (struct node));
	
	new_node -> data = new_data;
	
	new_node -> next = prev_node -> next;
	
	prev_node -> next = new_node;
	
	new_node -> prev = prev_node;
	
	if(new_node -> next != NULL)
	{
		new_node -> next -> prev = new_node;
	}
}

//============================================================================//
// insere um noh no final
void insert_end(struct node** head_ref, int new_data)
{
	struct node* new_node = (struct node*)malloc(sizeof(struct node))
	struct node* last = *head_ref;
	
	new_node -> data = new_data;
	
	new_node -> next = NULL;
	
	if(*head_ref == NULL)
	{
		new_node -> prev == NULL;
		*head_ref = new_node;
		return;
	}
	while(last -> next != NULL)
	{
		last = last -> next;
	}
	last -> next = new_node;
	new_node -> prev = last;
	return;
}

//============================================================================//
// inserir antes de um no fornecido
void insertBefore(struct node** head_ref, struct node* next_node, int new_data)
{
	if(next_node == NULL)
	{
		return;
	}
	
	struct node* new_node = (struct node*)malloc(sizeof(struct node));
	
	new_node -> data = new_data;
	
	new_node -> prev = next_node -> prev
	
	next_node -> prev = new_node;
	
	new_node -> next = next_node;
	
	if(new_node -> prev != NULL)
	{
		new_node -> prev -> next = new_node;
	}
	
	else
	{
		(*head_ref) = new_node;
	}
}